use std::marker::PhantomData;

use type_name;

use specs::{Entities, Entity, Join, System, WriteStorage};

use super::Messenger;

pub struct MessengerSystem<T>(PhantomData<T>);

impl<T> Default for MessengerSystem<T> {
    #[inline(always)]
    fn default() -> Self {
        MessengerSystem::new()
    }
}

impl<T> MessengerSystem<T> {
    #[inline(always)]
    pub fn new() -> Self {
        MessengerSystem(PhantomData)
    }
    #[inline]
    pub fn name() -> &'static str {
        type_name::get::<Self>()
    }
}

impl<'system, T> System<'system> for MessengerSystem<T>
where
    T: 'static + Send + Sync + Clone,
{
    type SystemData = (Entities<'system>, WriteStorage<'system, Messenger<T>>);

    #[inline]
    fn run(&mut self, (entities, mut messengers): Self::SystemData) {
        let mut messages: Vec<(Entity, Entity, T)> = Vec::new();

        {
            let mut entity_messengers: Vec<(Entity, &mut Messenger<T>)> =
                (&*entities, &mut messengers).join().collect();

            let len = entity_messengers.len();

            for i in 0..len {
                let (from_entity, broadcasting): (Entity, Vec<T>) = {
                    let (from_entity, messenger) = &mut entity_messengers[i];

                    for (to_entity, message) in messenger.outgoing.drain(..) {
                        messages.push((*from_entity, to_entity, message));
                    }

                    (*from_entity, messenger.broadcasting.drain(..).collect())
                };

                for message in broadcasting {
                    for j in 0..len {
                        let (to_entity, _) = &mut entity_messengers[j];

                        if i != j {
                            messages.push((from_entity, *to_entity, message.clone()));
                        }
                    }
                }
            }
        }

        for (from_entity, to_entity, message) in messages.drain(..) {
            if let Some(messenger) = messengers.get_mut(to_entity) {
                messenger.incoming.push((from_entity, message));
            }
        }
    }
}

#[test]
fn test_messenger_system_name() {
    assert_eq!(
        MessengerSystem::<String>::name(),
        "messenger_system::MessengerSystem<std::string::String>"
    );

    #[derive(Clone)]
    struct Example;
    assert_eq!(
        MessengerSystem::<Example>::name(),
        "messenger_system::MessengerSystem<messenger_system::test_messenger_system_name::Example>"
    );
}
