use std::marker::PhantomData;

use specs::{DispatcherBuilder, World};
use specs_bundler::SpecsBundle;

use super::{Messenger, MessengerSystem};

pub struct MessengerBundle<T>(PhantomData<T>);

impl<T> Default for MessengerBundle<T> {
    #[inline(always)]
    fn default() -> Self {
        Self::new()
    }
}

impl<T> MessengerBundle<T> {
    #[inline(always)]
    pub fn new() -> Self {
        MessengerBundle(PhantomData)
    }
}

impl<'a, 'b, T> SpecsBundle<'a, 'b> for MessengerBundle<T>
where
    T: 'static + Send + Sync + Clone,
{
    type Error = ();

    #[inline]
    fn build(
        self,
        world: &mut World,
        dispatch_builder: DispatcherBuilder<'a, 'b>,
    ) -> Result<DispatcherBuilder<'a, 'b>, Self::Error> {
        world.register::<Messenger<T>>();

        Ok(dispatch_builder.with(
            MessengerSystem::<T>::new(),
            &MessengerSystem::<T>::name(),
            &[],
        ))
    }
}
