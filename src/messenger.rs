use std::slice::{Iter, IterMut};
use std::vec::Drain;

use specs::{Component, Entity, VecStorage};

#[derive(Debug)]
pub struct Messenger<T> {
    pub(super) broadcasting: Vec<T>,
    pub(super) outgoing: Vec<(Entity, T)>,
    pub(super) incoming: Vec<(Entity, T)>,
}

impl<T> Component for Messenger<T>
where
    T: 'static + Sync + Send,
{
    type Storage = VecStorage<Self>;
}

impl<T> Default for Messenger<T> {
    #[inline(always)]
    fn default() -> Self {
        Messenger {
            broadcasting: Vec::new(),
            incoming: Vec::new(),
            outgoing: Vec::new(),
        }
    }
}

impl<T> Messenger<T>
where
    T: Clone,
{
    #[inline(always)]
    pub fn new() -> Self {
        Self::default()
    }

    #[inline(always)]
    pub fn send(&mut self, to: Entity, msg: T) -> &mut Self {
        self.outgoing.push((to, msg));
        self
    }

    #[inline(always)]
    pub fn broadcast(&mut self, msg: T) -> &mut Self {
        self.broadcasting.push(msg);
        self
    }

    #[inline(always)]
    pub fn incoming_ref(&self) -> Iter<(Entity, T)> {
        self.incoming.iter()
    }

    #[inline(always)]
    pub fn incoming_mut(&mut self) -> IterMut<(Entity, T)> {
        self.incoming.iter_mut()
    }

    #[inline(always)]
    pub fn incoming(&mut self) -> Drain<(Entity, T)> {
        self.incoming.drain(..)
    }
}
