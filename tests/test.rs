extern crate specs;
extern crate specs_bundler;
extern crate specs_messenger;

use specs::{DispatcherBuilder, Entities, Entity, Join, System, World, Write, WriteStorage};
use specs_bundler::SpecsBundler;
use specs_messenger::{Messenger, MessengerBundle, MessengerSystem};

pub struct TestSystem;

impl Default for TestSystem {
    #[inline(always)]
    fn default() -> Self {
        Self::new()
    }
}

impl TestSystem {
    #[inline(always)]
    pub fn new() -> Self {
        TestSystem
    }
}

impl<'system> System<'system> for TestSystem {
    type SystemData = (
        Entities<'system>,
        Write<'system, Vec<(Entity, Entity, String)>>,
        WriteStorage<'system, Messenger<String>>,
    );

    fn run(&mut self, (entities, mut messages, mut messengers): Self::SystemData) {
        for (to_entity, messenger) in (&*entities, &mut messengers).join() {
            for (from_entity, message) in messenger.incoming() {
                messages.push((from_entity, to_entity, message));
            }
        }
    }
}

#[test]
fn test_messenger() {
    let mut world = World::new();

    let mut dispatcher = SpecsBundler::new(&mut world, DispatcherBuilder::new())
        .bundle(MessengerBundle::<String>::new())
        .unwrap()
        .with(
            TestSystem::new(),
            "test_system",
            &[MessengerSystem::<String>::name()],
        )
        .build();

    let entity0 = world
        .create_entity()
        .with(Messenger::<String>::new())
        .build();

    let entity1 = world
        .create_entity()
        .with(Messenger::<String>::new())
        .build();

    let entity2 = world
        .create_entity()
        .with(Messenger::<String>::new())
        .build();

    world.add_resource::<Vec<(Entity, Entity, String)>>(Vec::new());

    {
        let mut messengers = world.write_storage::<Messenger<String>>();

        messengers
            .get_mut(entity0)
            .unwrap()
            .send(entity1, "Direct".into());

        messengers
            .get_mut(entity2)
            .unwrap()
            .broadcast("Broadcast".into());
    }

    dispatcher.dispatch(&mut world.res);

    let messages = world.read_resource::<Vec<(Entity, Entity, String)>>();

    assert_eq!(messages[0], (entity2, entity0, "Broadcast".to_string()));
    assert_eq!(messages[1], (entity0, entity1, "Direct".to_string()));
    assert_eq!(messages[2], (entity2, entity1, "Broadcast".to_string()));
}
