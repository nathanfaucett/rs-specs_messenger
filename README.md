# specs_messenger

message passing component for specs

```rust
let mut world = World::new();

let mut dispatcher = SpecsBundler::new(&mut world, DispatcherBuilder::new())
    .bundle(MessengerBundle::<String>::new()).unwrap()
    .build();

let entity0 = world
    .create_entity()
    .with(Messenger::<String>::new())
    .build();

let entity1 = world
    .create_entity()
    .with(Messenger::<String>::new())
    .build();

let entity2 = world
    .create_entity()
    .with(Messenger::<String>::new())
    .build();

{
    let mut messengers = world.write_storage::<Messenger<String>>();

    messengers
        .get_mut(entity0)
        .unwrap()
        .send(entity1, "Direct".into());

    messengers
        .get_mut(entity2)
        .unwrap()
        .broadcast("Broadcast".into());
}

dispatcher.dispatch(&mut world.res);
```
